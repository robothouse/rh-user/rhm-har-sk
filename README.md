# RHM-HAR-SK Dataset Grabber

The RHM-HAR-SK dataset, short for "Robot House Multi-View Human Activity Recognition by Skeleton-Based Data," contains pertinent information available in this [link](https://robothouse-dev.herts.ac.uk/datasets/RHM/HAR-SK).

## Description

The `LoakSK.py` file within this repository provides a dataset grabber for accessing skeleton data within the RHM-HAR-SK dataset. The dataset consists of three .pkl files, each containing skeleton data, bounding boxes, and confidence values, all sharing the same dimensions detailed below.

## Dataset Details

### Data Preparation from RH-HAR Dataset using Yolo7 Pose Extraction Method

The RH-HAR dataset comprises multi-view data encompassing 4 views and 14 distinct actions:

- `Bending`
- `SittingDown`
- `ClosingCan`
- `Reaching`
- `Walking`
- `Drinking`
- `StairsClimbingUp`
- `StairsClimbingDown`
- `StandingUp`
- `OpeningCan`
- `CarryingObject`
- `Cleaning`
- `PuttingDownObjects`
- `LiftingObject`

### Data Format and Structure

The skeleton data has been extracted from RGB videos and stored in the `RRH_HAR_SK_Yolo7` folder. Within this folder, you will find:

- All extracted skeletons stored in the `RH-HAR-SK.pkl` file.
- Confidence values for each skeleton stored in the `RH_HAR_confidence.pkl` file.
- Bounding boxes for each skeleton stored in the `RH_HAR_bbox.pkl` file.

### Dataset Structure 

The loaded data from `RH-HAR-SK.pkl` has the following dimensions:
- [4]: Four views
- [4, 14]: Fourteen actions in each view
- [4, 14, 433]: Number of samples in each action (in this case, 433 samples per action)
- [4, 14, 433, 118]: Number of frames in each sample (in this case, 118 frames per sample)
- [4, 14, 433, 118, 34]: Number of poses in each frame (17 poses per frame)

For each frame (34 elements)
- First 17 elements represent X-values
- Second 17 elements represent Y-values

Additionally, there is a `RH_HAR_confidence.pkl` file containing confidence values indexed similar to the skeleton data.

The dataset also includes bounding boxes (`RH_HAR_bbox.pkl`) for each person in each frame, stored in the same indices as the skeleton data except for the last index:
- [4]: Box tensor containing [xmin, ymin, xmax, ymax] for the bounding box coordinates.

**Note:** Bounding boxes are extracted for a single person in each frame.

## Usage
load the data using the following code:

in the default mode if detaset is located beside the code repository we can use the following code:

```angular2html
data = LoadSK() 
```
if the dataset is located in another path we can use the following code:

```angular2html
data = LoadSK(sk_path = ' the dataset path ')
```

to check the data we can use the following code:

```angular2html
print(f' number of views: {data.__len_views__()} ')
print(f' Number of action classes: {data.__len_actions__(0)} ')
print(f' Number of samples: {data.__len_samples__(0,0)} ')
print(f' Shape of sample: {data.__getitem__(0,0,0).shape} ')
print(f' Shape of confidence value of a sample: {data.__getcnf__(0,0,0).shape} ')
```

result:
```angular2html
number of views: 4 
Number of action classes: 14 
Number of samples: 433 
Shape of sample: torch.Size([118, 34]) 
Shape of confidence value of a sample: torch.Size([118, 34])
```

```angular2html
sample_sk = data.__getitem__(0,0,0)
print(f' skeleton data in a frame: {sample_sk[0]} ')

confidence = data.__getcnf__(0,0,0)
print(f' confidence value of a frame: {confidence[0]} ')

bounding_box = data.__getbbox__(0,0,0)
print(f' bounding box of a frame: {bounding_box[0]} ')
```

result:
```angular2html
skeleton data in a frame: tensor([390.2500, 394.7500, 388.5000, 413.7500, 393.2500, 442.7500, 394.0000,
        454.0000, 387.2500, 439.2500, 382.0000, 430.7500, 398.2500, 433.5000,
        401.2500, 460.2500, 405.7500, 274.2500, 268.0000, 268.5000, 265.5000,
        268.7500, 299.2500, 304.0000, 345.7500, 351.2500, 388.5000, 386.5000,
        399.2500, 399.5000, 468.7500, 471.0000, 531.5000, 541.5000]) 
 confidence value of a frame: tensor([0.9639, 0.9858, 0.4709, 0.9902, 0.0877, 0.9834, 0.9385, 0.9683, 0.6167,
        0.9478, 0.5933, 0.9863, 0.9722, 0.9819, 0.9604, 0.9575, 0.9272, 0.9639,
        0.9858, 0.4709, 0.9902, 0.0877, 0.9834, 0.9385, 0.9683, 0.6167, 0.9478,
        0.5933, 0.9863, 0.9722, 0.9819, 0.9604, 0.9575, 0.9272]) 
 bounding box of a frame: tensor([370.4375, 239.5625, 473.5625, 565.8125], dtype=torch.float64)
```

The example of the dataset usage is provided in the `Loading-skeleton-example.ipynb` file in the repository.

## Citation

Mohamad Reza Shahabian Alashti, Mohammad Hossein Bamorovat Abadi, Patrick Holthaus, Catherine Menon, and Farshid Amirabdollahian. RHM-HAR-SK: A Multiview Dataset with Skeleton Data for Ambient Assisted Living Research. In The Sixteenth International Conference on Advances in Computer-Human Interactions (ACHI 2023). 181-193. Venice, Italy, IARIA.
BibTeX

@inproceedings{Shahabian2023a,
    author = "Shahabian Alashti, Mohamad Reza and Bamorovat Abadi, Mohammad Hossein and Holthaus, Patrick and Menon, Catherine and Amirabdollahian, Farshid",
    title = "{RHM-HAR-SK: A Multiview Dataset with Skeleton Data for Ambient Assisted Living Research}",
    booktitle = "The Sixteenth International Conference on Advances in Computer-Human Interactions (ACHI 2023)",
    address = "Venice, Italy",
    year = "2023",
    publisher = "IARIA",
    isbn = "978-1-68558-078-0",
    pages = "181--187",
    url = "https://www.thinkmind.org/index.php?view=article\&articleid=achi\_2023\_4\_190\_20087"
}
